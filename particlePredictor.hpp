#pragma once

#include <math.h>
#include <vector>
#include <random>
#include <algorithm>
#include <Eigen/Dense>
#include "cfmodels.hpp"
#include "ziggurat.hpp"

namespace cfmodels {

const auto log = fastlog;
const auto exp = fastexp;

real radians(real angle) {
	return angle*M_PI/180.0;
}

real degrees(real radians) {
	return radians/M_PI*180.0;
}

auto sign(auto x) {
	return (x > 0) - (x < 0);
}

Ziggurat::NormalRng standard_normal_rnd;

/*
struct RandomNormal {
	real mean;
	real std;
	Ziggurat::NormalRng gen;

	RandomNormal(real mean, real std) : mean(mean), std(std), gen(1) {

	}

	real operator()() {
		return standard_normal_rng.norm()*std + mean;
	}
};*/

std::default_random_engine generator(1); // TODO: NO globals!
struct NormalDistribution {
	real mean;
	real std;
	//std::normal_distribution<real> gen;
	//RandomNormal gen;
	

	NormalDistribution(real mean, real std) :mean(mean), std(std) {}

	real random() {
		return standard_normal_rnd.norm()*std + mean;
	}

	real logpdf(real x) {
		return -log(std*sqrt(2*M_PI)) - pow(x - mean, 2)/(2*std*std);
	}
};

/*
struct LaplaceDistribution {
	real loc;
	real scale;
	std::uniform_real_distribution<real> gen;

	LaplaceDistribution(real loc, real scale) :loc(loc), scale(scale), gen(-0.5, 0.5) {}

	real random() {
		real u = gen(generator);
		return loc - scale*sign(u)*(1 - 2*std::abs(u));
	}

	real logpdf(real x) {
		real d = std::abs(x - loc);
		return -log(2*scale) - d/scale;
	}
};
*/

struct FollowState {
	real ownspeed;
	real gap;
	real leader_speed;
	
	FollowState(real ownspeed, real gap, real leader_speed)
		:ownspeed(ownspeed), gap(gap), leader_speed(leader_speed) {
	}

	real relspeed() {
		return leader_speed - ownspeed;
	}
};

struct FollowStateHypothesis {
	FollowState state;
	real loglik;
	real weight;

	FollowStateHypothesis(FollowState state)
		:state(state), loglik(0.0), weight(0.0) {}
};

real cartesian_to_angle_speed(real dv, real w, real g) {
	return -(4*w*dv)/(4*g*g + w*w);
}


typedef Eigen::Map<Eigen::ArrayXd, 0, Eigen::InnerStride<sizeof(FollowStateHypothesis)/sizeof(real)>> ParticleFieldMap;

real hacklog(real v) {
	// A hack for logarithmic values that may
	// get sampled at negative
	return log(std::max(v, 0.001));
}

struct FollowStatePredictor {
	std::vector<FollowStateHypothesis> particles;
	
	NormalDistribution leader_volatility;
	NormalDistribution accel_noise;

	NormalDistribution ownspeed_measurement;
	NormalDistribution angle_measurement;
	NormalDistribution angle_speed_measurement;


	real vehicle_width = 2.0;
	real driver_position = -2.0;

	FollowStatePredictor(FollowState initialState, size_t n_particles=1024)
		:particles(n_particles, initialState),
		leader_volatility(0.0, 4.0),
		accel_noise(0.0, 0.3),
		ownspeed_measurement(0.0, 0.3),
		angle_measurement(0.0, radians(0.3)),
		angle_speed_measurement(0.0, radians(0.3))
	{
	}

	void predict(real dt, real accel) {
		for(auto& p : particles) {
			p.state.gap += p.state.relspeed()*dt;
			p.state.leader_speed += leader_volatility.random()*dt;
			//p.state.leader_speed = std::max(p.state.leader_speed, 0.0);
			
			//p.state.ownspeed += (accel + accel_noise.random())*dt;
			p.state.ownspeed += (accel_noise.random()*accel + accel)*dt;
			//p.state.ownspeed = std::max(p.state.ownspeed, 0.0);

		}
	}

	void predict_known(real dt, const FollowState& state) {
		static NormalDistribution speed_proposal(0.0, 3.0);
		static NormalDistribution gap_proposal(0.0, 10.0);
		for(auto&p : particles) {
			p.state.gap += p.state.relspeed()*dt;
			
			auto prevState = p.state;
			//p.state.ownspeed += accel_noise.random()*dt;
			auto osd = speed_proposal.random();
			p.loglik -= speed_proposal.logpdf(osd);
			p.state.ownspeed = state.ownspeed + osd;
			p.loglik += accel_noise.logpdf((p.state.ownspeed - prevState.ownspeed)/dt);
			
			auto lsd = speed_proposal.random();
			p.loglik -= speed_proposal.logpdf(lsd);
			p.state.leader_speed = state.leader_speed + lsd;
			p.loglik += leader_volatility.logpdf((p.state.leader_speed - prevState.leader_speed)/dt);
			//p.state.leader_speed += leader_volatility.random()*dt;
			
			//auto gd = gap_proposal.random();
			//p.loglik -= gap_proposal.logpdf(gd);
			//p.state.gap = std::exp(std::log(state.gap) + gd);
			//p.state.gap = state.gap + gd;
			
			//auto relspeed = (p.state.gap - prevState.gap)/dt;
			//p.state.leader_speed = relspeed - prevState.ownspeed;
			//p.loglik += leader_volatility.logpdf((p.state.leader_speed - prevState.leader_speed)/dt);
		}
	}

	void measure_ownspeed(real v) {
		real logv = hacklog(v);
		logv += ownspeed_measurement.random();
		for(auto& p: particles) {
			auto diff = logv - hacklog(p.state.ownspeed);
			//p.loglik += ownspeed_measurement.logpdf( (diff + 1.0)/(std::abs(p.state.ownspeed) + 1.0)*0.1 );
			p.loglik += ownspeed_measurement.logpdf(diff);
		}
	}
	
	void measure_gap(real gap) {
		real angle = 2*std::atan((vehicle_width/2)/(gap - driver_position));
		angle += angle_measurement.random();
		for(auto& p: particles) {
			real pred_angle = 2*std::atan((vehicle_width/2)/(p.state.gap - driver_position));
			p.loglik += angle_measurement.logpdf(angle - pred_angle);
		}
	}

	void measure_relspeed(real relspeed, real gap) {
		auto w = vehicle_width;
		real angle_speed = cartesian_to_angle_speed(relspeed, w, gap - driver_position);
		angle_speed += angle_speed_measurement.random();
		for(auto& p: particles) {
			real pred_angle_speed = cartesian_to_angle_speed(p.state.relspeed(), w, p.state.gap - driver_position);
			p.loglik += angle_speed_measurement.logpdf(angle_speed - pred_angle_speed);
		}
	}
	
	void normalize_weights() {
		real maxlik = std::max_element(particles.begin(), particles.end(),
				[](auto& a, auto& b) { return a.loglik < b.loglik; })->loglik;
		real expsum = 0.0;
		for(auto& p : particles) {
			p.loglik -= maxlik;
			p.weight = exp(p.loglik);
			expsum += p.weight;
		}

		for(auto& p : particles) {
			p.weight /= expsum;
		}

	}

	void finish_step() {
		normalize_weights();		
		/*std::cout << ess << std::endl;
		if(ess < 0.5*particles.size()) {
			resample();
		}*/
		resample();
	}

	real effective_sample_size() {
		auto w = weights();
		return square(w.sum())/square(w).sum();
	}

	void resample() {
		// TODO: Denumpify!
		size_t N = particles.size();
		auto orig = particles;
		std::uniform_real_distribution(sampler);
		auto offset = std::uniform_real_distribution<real>()(generator);
		std::vector<real> positions(N);
		std::vector<real> cumweights(N);
		real cum = 0.0;
		for(size_t i = 0; i < N; ++i) {
			positions[i] = (offset + i)/N;
			cum += particles[i].weight;
			cumweights[i] = cum;
		}
		size_t i = 0, j = 0;
		while(i < N) {
			if(positions[i] < cumweights[j]) {
				particles[i] = orig[j];
				particles[i].loglik = log(1.0/N);
				particles[i].weight = 1.0/N;
				i++;
			} else {
				j++;
			}
		}

	}
	
	ParticleFieldMap weights() { return ParticleFieldMap(&particles[0].weight, particles.size()); }
	ParticleFieldMap logliks() { return ParticleFieldMap(&particles[0].loglik, particles.size()); }
	ParticleFieldMap ownspeeds() { return ParticleFieldMap(&particles[0].state.ownspeed, particles.size()); }
	ParticleFieldMap leader_speeds() { return ParticleFieldMap(&particles[0].state.leader_speed, particles.size()); }
	ParticleFieldMap gaps() { return ParticleFieldMap(&particles[0].state.gap, particles.size()); }
};

struct DistractedAccelerator : public Accelerator {
	real accel_threshold;
	shared_ptr<FollowStatePredictor> predictor;
	shared_ptr<Accelerator> accelerator;

	real prev_accel;
	NormalDistribution accel_noise;
	
	real glance_left=0.0;
	real glance_duration;
	// HACK!
	std::vector<bool> glances;

	DistractedAccelerator(real accel_threshold, shared_ptr<FollowStatePredictor> pred, shared_ptr<Accelerator> accel)
		:accel_threshold(accel_threshold), predictor(pred), accelerator(accel), prev_accel(0.0), accel_noise(0.0, 1.0), glance_duration(0.3) {
	}

	real operator()(real dt, real v, real dx, real dv) {
		predictor->predict(dt, prev_accel);
		predictor->measure_ownspeed(v);
		if(glance_left > 0.0) {
			glance_left -= dt;
			predictor->measure_gap(dx);
			predictor->measure_relspeed(dv, dx);
		}
		predictor->finish_step();

		//accelerations.resize(predictor->particles.size());
		real mean_accel = 0;
		real accel_s = 0;
		real total_weight = 0;
		for(size_t i=0; i < predictor->particles.size(); ++i) {
			auto particle = predictor->particles[i];
			auto state = particle.state;
			auto accel = (*accelerator)(dt, state.ownspeed, state.gap, state.relspeed());
			auto weight = particle.weight;
			total_weight += weight;
			auto delta = (accel - mean_accel);
			mean_accel += weight/total_weight*delta;
			accel_s += weight*delta*(accel - mean_accel);
		}

		accel_s = std::sqrt(accel_s/total_weight);
		
		if(accel_s > accel_threshold && glance_left <= 0.0) {
			glance_left = glance_duration;
			glances.push_back(true);
		} else {
			glances.push_back(false);
		}

		/*if(std::abs(prev_accel - mean_accel) > 1.0) {
			prev_accel = mean_accel;
		}*/
		prev_accel = mean_accel;

		return prev_accel;
	}
};

/*struct ActionPointAccelerator : public Accelerator {
	
}*/

}
