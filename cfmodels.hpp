#pragma once

#include <vector>
#include <algorithm>
#include <cmath>
#include <limits>
#include <functional>
#include <memory>
#include <iostream>
#include <queue>
#include "fastonebigheader.h"

namespace cfmodels {
typedef double real;

using std::vector;
using std::shared_ptr;
using std::make_shared;
using std::sqrt;


auto square(auto a) {
	return a*a;
}

// See: https://martin.ankerl.com/2007/10/04/optimized-pow-approximation-for-java-and-c-c/
// std::pow is really really slow
double fastPow(double a, double b) {
  union {
    double d;
    int x[2];
  } u = { a };
  u.x[1] = (int)(b * (u.x[1] - 1072632447) + 1072632447);
  u.x[0] = 0;
  return u.d;
}

struct Accelerator {
	virtual real operator()(real dt, real v, real dx, real dv) = 0;
};

struct Measurement {
	real v, dx, dv;
};

struct Idm : public Accelerator {
	real v0, T, a, b, aExp, s0, s1;
	Idm(real v0=120/3.6, real T=1.6, real a=0.73, real b=1.67, real aExp=4.0, real s0=2.0, real s1=0.0)
		: v0(v0), T(T), a(a), b(b), aExp(aExp), s0(s0), s1(s1)
	{
		
	}

	real operator()(real dt, real v, real dx, real dv) {
		v = std::max(0.0, v);
		dx = std::max(0.1, dx);
		auto desiredGap = s0 + s1*std::sqrt(v/v0) + T*v - (v*dv)/(2*std::sqrt(a*b));
		return a*(1.0 - fastPow(v/v0, aExp) - square(desiredGap/dx));
	}

	real gradient_magnitude(real dt, real v, real dx, real dv) {
		// Differentiated using sympy
		real d_v = a*(-aExp*fastPow(v/v0, aExp)/v - (2*T + s1/(sqrt(v)*sqrt(v0)) - dv/(sqrt(a)*sqrt(b)))*(T*v + s0 + s1*sqrt(v)/sqrt(v0) - dv*v/(2*sqrt(a)*sqrt(b)))/(dx*dx));
		real d_dx = square(2*sqrt(a)*sqrt(b)*s1*sqrt(v) + 2*sqrt(a)*sqrt(b)*sqrt(v0)*(T*v + s0) - dv*v*sqrt(v0))/(2*b*dx*dx*dx*v0);
		real d_dv = v*(2*sqrt(a)*sqrt(b)*s1*sqrt(v) + 2*sqrt(a)*sqrt(b)*sqrt(v0)*(T*v + s0) - dv*v*sqrt(v0))/(2*b*dx*dx*sqrt(v0));

		return sqrt(d_v*d_v + d_dx*d_dx + d_dv*d_dv);
	}
};

struct LaggingAccelerator : public Accelerator {
	struct Sample {
		real t;
		real a;
	};
	shared_ptr<Accelerator> accel;
	real lag;
	real t;
	std::queue<Sample> history;
	real my_a = 0.0;

	LaggingAccelerator(real lag, shared_ptr<Accelerator> accel) :accel(accel), lag(lag), t(0.0) {
		
	}
	
	real operator()(real dt, real v, real dx, real dv) {
		t += dt;
		auto a = (*accel)(dt, v, dx, dv);
		/*if(prev_t == 0.0) {
			prev_t = t;
			prev_a = a;
		}

		history.push({t, a});
		while(history.front().t < (t - lag)) {
			prev_t = history.front().t;
			prev_a = history.front().a;
			history.pop();
		}
		auto lt = t - lag;

		return history.front().a;*/
	}
};

struct Fvdm : public Accelerator {
	real V1, V2, C1, C2, k, l;
	// Params from https://journals.aps.org/pre/pdf/10.1103/PhysRevE.64.017101
	Fvdm(real V1=6.75, real V2=7.91, real C1=0.13, real C2=1.57, real k=0.41, real l=0.4)
		:V1(V1), V2(V2), C1(C1), C2(C2), k(k), l(l) {
		
	}

	real optimal_velocity(real gap) {
		return V1 + V2*std::tanh(C1*gap - C2);
	}

	real operator()(real dt, real v, real dx, real dv) {
		return k*(optimal_velocity(dx) - v) + l*dv;
	}
};

struct PredictingAccelerator : public Accelerator {
	shared_ptr<Accelerator> accel;
	Measurement state;
	Measurement seen;
	real prediction_left = 0.0;
	real leader_speed = 0.0;
	real leader_accel = 0.0;
	real time_since_update = 0.0;

	PredictingAccelerator(shared_ptr<Accelerator> accel) : accel(accel) {
		
	}

	real operator()(real dt, real v, real dx, real dv) {
		time_since_update += dt;
		if(prediction_left <= 0.0) {
			leader_accel = ((dv + v) - (seen.dv + seen.v))/time_since_update;
			seen.v = v;
			seen.dx = dx;
			seen.dv = dv;

			
			leader_speed = dv + v;
			
			state = seen;
			time_since_update = 0.0;
			prediction_left = std::max(0.0, std::min(dx/(state.v + 1.0), 10.0));
			//prediction_left = 1.0;
		} else {
			if(leader_accel < 0) {
				leader_speed += leader_accel*dt;
			} else {
				leader_speed += leader_accel*dt;
			}
			state.v = v;
			state.dv = leader_speed - v;
			state.dx += dv*dt;
			prediction_left -= dt;
		}

		return (*accel)(dt, state.v, state.dx, state.dv);
	}
};

struct Vehicle {
	real position = 0.0;
	real velocity = 0.0;
	shared_ptr<Accelerator> acceleration;

	Vehicle(shared_ptr<Accelerator> a) : acceleration(a) {}
};

struct LoopEnvironment
{
	vector<Vehicle> vehicles;
	real length;
	LoopEnvironment(real length=1000.0)
		:length(length)
	{
		
	}

	void spread_equally() {
		auto n = vehicles.size();
		for(size_t i = 0; i < n; ++i) {
			vehicles[i].position = i/real(n)*length;
		}
		
		std::sort(vehicles.begin(), vehicles.end(),
			[](const auto& a, const auto& b) { return a.position < b.position; }
		);

	}

	void add_vehicle(shared_ptr<Accelerator> a) {
		vehicles.emplace_back(a);
	}

	void step(real dt) {
		for(auto& v : vehicles) {
			v.position = std::fmod(v.position, length);
		}
		
		auto n = vehicles.size();
		for(size_t i = 0; i < n; ++i) {
			auto& v = vehicles[i];
			auto& leading_v = vehicles[(i+1)%n];
			auto dx = leading_v.position - v.position;
			auto dv = leading_v.velocity - v.velocity;
			
			real normalizer = 0.0;
			if(dx < 0 || n < 2) {
				normalizer = length;
				dx += normalizer;
			}

			auto accel = (*v.acceleration)(dt, v.velocity, dx, dv);
			v.velocity += accel*dt;
			if(v.velocity < 0.0) {
				v.velocity = 0.0;
			}

			v.position += v.velocity*dt;

			dx = (leading_v.position + normalizer) - (v.position);
			
			if(dx < 0 && n > 1) {
				// Oops, a crash!
				std::cout << "Crash!" << std::endl;
				v.position = leading_v.position;
				v.velocity = 0.0;
			}
		}
	}
};

vector<real> simulateLoop(LoopEnvironment& env, real duration=10*60, real dt=0.1)
{
	size_t n_steps = duration/dt;
	vector<real> meanspeeds;
	
	for(size_t i=0; i < n_steps; ++i) {
		env.step(dt);
		auto mspeed = 0.0;
		for(auto v : env.vehicles) {
			mspeed += v.velocity/env.vehicles.size();
		}

		meanspeeds.push_back(mspeed);
	}

	return meanspeeds;
}

auto simulateFollower(Accelerator& model, real x, real v, const vector<real> ts, const vector<real> leader, const vector<real> leader_speed) {
	auto length = ts.size();
	vector<real> xs(length);
	real vehicle_length = 4.5;
	
	xs[0] = x;
	for(size_t i=1; i < length; ++i) {
		auto dt = ts[i] - ts[i-1];
		auto lv = leader_speed[i];
		auto dv = lv - v;
		auto gap = leader[i] - x - vehicle_length;
		auto a = model(dt, v, gap, dv);
	
		v += a*dt;
		x += v*dt;
		
		gap = leader[i] - x - vehicle_length;
		
		if(gap < 0) {
			x = leader[i] - vehicle_length;
			v = 0.0;
		}
		xs[i] = x;
	}

	return xs;
}
}
