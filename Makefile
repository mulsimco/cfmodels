benchmark_pfm: benchmark_pfm.cpp cfmodels.hpp particlePredictor.hpp
	g++ -fopenmp -std=c++17 -DWITHGPERFTOOLS -lprofiler `pkg-config --cflags eigen3` -g -Ofast $< -o $@

benchmark: benchmark.cpp cfmodels.hpp
	g++ -g -Ofast -ffast-math $< -o $@
