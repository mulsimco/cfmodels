#!/usr/bin/env python3

from __init__ import *

import numpy as np

n_vehicles = 100
dt = 0.1
duration = 60*10
densities = np.linspace(1, 50, 10)
speeds = []

def simulate(density, vgen):
    length = (1000*n_vehicles)/density
    env = LoopEnvironment(length)
    for i in range(n_vehicles):
        env.add_vehicle(vgen())
    env.spread_equally()
    return np.mean(simulateLoop(env, dt=dt, duration=duration))

import matplotlib.pyplot as plt

def plot_fundamentals(vgen, label=None, iters=10):
    speeds = []
    for d in densities:
        dspeed = 0.0
        for i in range(iters):
            dspeed += simulate(d, vgen)/iters
        speeds.append(dspeed)

    speeds = np.array(speeds)*3.6
    flows = np.multiply(densities, speeds)
    plt.subplot(3,1,1)
    plt.plot(densities, speeds, '-', alpha=0.7, label=label)
    plt.xlabel('Density'); plt.ylabel('Speed')
    plt.subplot(3,1,2)
    plt.plot(densities, flows, '-', alpha=0.7)
    plt.xlabel('Density'); plt.ylabel('Flow')
    plt.subplot(3,1,3)
    plt.plot(flows, speeds, '-', alpha=0.7)
    plt.xlabel('Flow'); plt.ylabel('Speed')

s0 = 5.0 + 5.0
T = 2.0


def hybrid(p):
    if np.random.random() < p:
        return Idm(T=1.0, s0=s0)
    else:
        return PredictingAccelerator(Idm(T=T, s0=s0))

#for p in np.linspace(0.0, 1.0, 6):
#    plot_fundamentals(lambda: hybrid(p), 'p=%.1f'%(p))

for v0 in [120/3.6]:
    plot_fundamentals(lambda: Idm(T=T, s0=s0, v0=v0), 'Vanilla %.1f'%(v0*3.6))
    plot_fundamentals(lambda: PredictingAccelerator(Idm(T=T, s0=s0, v0=v0)), 'Pred %.1f'%(v0*3.6))
plt.subplot(3,1,1); plt.legend()
plt.show()

