import cppimport
cppimport.set_quiet(False)
# A hack required to get relative imports working
# with cppimport
import sys
import os
sys.path.insert(0, os.path.dirname(__file__))
try:
    cppimport.imp('_pycfmodels')
#    cppimport.imp('_theia')
finally:
    sys.path.pop(0)
from _pycfmodels import *
#import _theia as theia

"""
def Idm(v0=120.0/3.6, T=1.6, a=0.73, b=1.67, aExp=4.0, s0=2.0, s1=0.0):
    def accel(ts, v, gap, dv):
        if a*b < 0:
            return np.array([np.nan]*len(ts))
        desiredGap = s0 + s1*np.sqrt(v/v0) + T*v + (v*dv)/(2*np.sqrt(a*b))
        return a*(1 - (v/v0)**aExp - (desiredGap/gap)**2)
    return accel
"""
