#include "cfmodels.hpp"
#include <chrono>
typedef std::chrono::high_resolution_clock Clock;

int main() {
	size_t iters = 1000;
	size_t n_vehicles = 10;
	real duration = 10*60;
	auto start = Clock::now();
	for(size_t i = 0; i < iters; ++i) {
		LoopEnvironment env(1000.0);
		for(size_t j=0; j < n_vehicles; ++j) {
			auto accel = make_shared<PredictingAccelerator>(make_shared<Idm>());
			env.add_vehicle(accel);
		}
		env.spread_equally();
		simulateLoop(env, duration);
	}

	auto cpudur = Clock::now() - start;
	auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(cpudur).count();
	auto simtime = iters*duration*1000;
	std::cout << simtime/double(ms)*n_vehicles << " vehicle-seconds per second" << std::endl;
	//std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(duration).count()/float(iters) << std::endl;
}
