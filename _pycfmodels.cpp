<%
#cfg['libraries'] = ['ceres']
cfg['include_dirs'] = [
	'/usr/include/eigen3'
	]
cfg['dependencies'] = ['cfmodels.hpp', 'particlePredictor.hpp']
setup_pybind11(cfg)
cfg['compiler_args'] = ['-std=c++1z', '-g', '-Ofast', '-UNDEBUG', '-Wno-misleading-indentation']
%>


#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

#include "cfmodels.hpp"
#include "particlePredictor.hpp"

using namespace cfmodels;

template <typename T, typename ...Args>
T* _New(Args ...args) {
	return new T(args...);
}


PYBIND11_MODULE(_pycfmodels, m) {
namespace py = pybind11;
using namespace pybind11::literals;

py::class_<Accelerator, shared_ptr<Accelerator>>(m, "Accelerator")
.def("__call__", &Accelerator::operator())
.def("vec", py::vectorize(&Accelerator::operator()))
;

py::class_<Idm, Accelerator, shared_ptr<Idm>>(m, "Idm")
.def(py::init<real, real, real, real, real, real, real>(),
	"v0"_a=120/3.6, "T"_a=1.6, "a"_a=0.73, "b"_a=1.67,
	"aExp"_a=4.0, "s0"_a=2.0, "s1"_a=0.0
	)
.def("gradient_magnitude", &Idm::gradient_magnitude)
;

py::class_<Fvdm, Accelerator, shared_ptr<Fvdm>>(m, "Fvdm")
.def(py::init<real, real, real, real, real, real>(),
	"V1"_a=6.75, "V2"_a=7.91, "C1"_a=0.13, "C2"_a=1.57, "k"_a=0.41, "l"_a=0.4
	)
.def("optimal_velocity", py::vectorize(&Fvdm::optimal_velocity))
;

py::class_<PredictingAccelerator, Accelerator, shared_ptr<PredictingAccelerator>>(m, "PredictingAccelerator")
.def(py::init<shared_ptr<Accelerator>>())
;

py::class_<LaggingAccelerator, Accelerator, shared_ptr<LaggingAccelerator>>(m, "LaggingAccelerator")
.def(py::init<real, shared_ptr<Accelerator>>())
;

py::class_<DistractedAccelerator, Accelerator, shared_ptr<DistractedAccelerator>>(m, "DistractedAccelerator")
.def(py::init<real, shared_ptr<FollowStatePredictor>, shared_ptr<Accelerator>>())
.def_readonly("glances", &DistractedAccelerator::glances)
.def_readwrite("glance_left", &DistractedAccelerator::glance_left)
;

py::class_<LoopEnvironment>(m, "LoopEnvironment")
.def(py::init<real>(), "length"_a=1000.0)
.def("add_vehicle", &LoopEnvironment::add_vehicle)
.def("spread_equally", &LoopEnvironment::spread_equally)
;

py::class_<FollowState>(m, "FollowState")
.def(py::init<real, real, real>())
.def_readwrite("ownspeed", &FollowState::ownspeed)
.def_readwrite("gap", &FollowState::gap)
.def_readwrite("leader_speed", &FollowState::leader_speed)
.def("relspeed", &FollowState::relspeed)
;

py::class_<FollowStateHypothesis>(m, "FollowStateHypothesis")
.def_readonly("weight", &FollowStateHypothesis::weight)
.def_readonly("state", &FollowStateHypothesis::state)
;

py::class_<FollowStatePredictor, shared_ptr<FollowStatePredictor>>(m, "FollowStatePredictor")
.def(py::init<FollowState, size_t>())
.def_readonly("particles", &FollowStatePredictor::particles)
.def("predict", &FollowStatePredictor::predict)
.def("predict_known", &FollowStatePredictor::predict_known)
.def("measure_ownspeed", &FollowStatePredictor::measure_ownspeed)
.def("measure_gap", &FollowStatePredictor::measure_gap)
.def("measure_relspeed", &FollowStatePredictor::measure_relspeed)
.def("finish_step", &FollowStatePredictor::finish_step)
.def("clone", [](const FollowStatePredictor& p) { return p; })
.def("weights", &FollowStatePredictor::weights)
.def("logliks", &FollowStatePredictor::logliks)
.def("normalize_weights", &FollowStatePredictor::normalize_weights)
.def("ownspeeds", &FollowStatePredictor::ownspeeds)
.def("leader_speeds", &FollowStatePredictor::leader_speeds)
.def("gaps", &FollowStatePredictor::gaps)
.def_readonly("leader_volatility", &FollowStatePredictor::leader_volatility)
;

py::class_<NormalDistribution>(m, "NormalDistribution")
//.def(py::init<NormalDistribution, real, real>())
.def_readwrite("mean", &NormalDistribution::mean)
.def_readwrite("std", &NormalDistribution::std)
;

m.def("simulateLoop", simulateLoop, "env"_a, "duration"_a=10*60, "dt"_a=0.1);
m.def("simulateFollower", &simulateFollower);
m.def("simulateFollower_nogil", &simulateFollower, py::call_guard<py::gil_scoped_release>());
}
