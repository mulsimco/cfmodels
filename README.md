# Assorted Car Following models

For the model described in "A computational model for driver's cognitive state, visual perception and intermittent attention in a distracted car following task",
see `particlePredictor.hpp` and for usage see `benchmark_pfm.cpp`.

Released under the GNU AGPL-3.0, copyleft Jami Pekkanen 2018 unless otherwise noted in the code.
